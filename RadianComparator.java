import java.util.Comparator;

public class RadianComparator implements Comparator<Double> {
    private static final double PRECISION = 0.001;
    private static final double TWO_PI = 2 * Math.PI;

    private double normalize(double value) {
        value %= TWO_PI;
        if (value < 0) {
            value += TWO_PI;
        }
        return value;
    }

    @Override
    public int compare(Double a, Double b) {
        // Обработка случая с null значениями
        if (a == null && b == null) {
            return 0;
        }
        if (a == null) {
            return -1;
        }
        if (b == null) {
            return 1;
        }

        // Нормализация значений в пределах [0..2π)
        double normalizedA = normalize(a);
        double normalizedB = normalize(b);

        // Сравнение нормализованных значений с учетом допустимой погрешности
        if (Math.abs(normalizedA - normalizedB) < PRECISION) {
            return 0;
        } else if (normalizedA < normalizedB) {
            return -1;
        } else {
            return 1;
        }
    }

    public static RadianComparator get() {
        return new RadianComparator();
    }





    public static void main(String[] args) {
        Comparator<Double> cmp = RadianComparator.get();

        System.out.println(cmp.compare(0.0, 0.0));                 //   0
        System.out.println(cmp.compare(0.0, 0.0005));             //   0
        System.out.println(cmp.compare(0.0, 2 * Math.PI));        //   0
        System.out.println(cmp.compare(0.0, Math.PI));            // < 0
        System.out.println(cmp.compare(Math.PI, 0.0));            // > 0
        System.out.println(cmp.compare(Math.PI, 3 * Math.PI));    //   0
        System.out.println(cmp.compare(Math.PI, 4 * Math.PI));    // > 0
    }
}
